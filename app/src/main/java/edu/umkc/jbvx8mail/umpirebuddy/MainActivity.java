package edu.umkc.jbvx8mail.umpirebuddy;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public final static String EXTRA_DATA = "edu.umkc.jbvx8.umpirebuddy.ABOUTDATA";
    private static final String TAG = "Umpire Buddy";
    private static final String PREFERENCE_NAME = "PreferenceFile";

    private int strike_count = 0;
    private int ball_count = 0;
    private int out_count = 0;
/*
    private ActionMode mActionMode;
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.menu_context, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.contextStrikes:
                    strike_count++;
                    if (strike_count == 3) {
                        out_count++;
                        // Builder is an inner class so we have to qualify it
                        // with its outer class: AlertDialog
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        //builder.setTitle("Dialog box");
                        builder.setMessage("3 strikes, out! New batter?");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                strike_count = 0;
                                ball_count = 0;
                                // Note, you have to call update count here because.
                                //   the call builder.show() below is non blocking.
                                updateBalls();
                                updateStrikes();
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // Do nothing
                                finish();
                            }
                        });
                        builder.show();
                    }

                    mode.finish(); // Action picked, so close the Contextual Action Bar(CAB)
                    return true;
                case R.id.contextBalls:
                    ball_count++;
                    if (ball_count == 4) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        //builder.setTitle("Dialog box");
                        builder.setMessage("4 balls, walk. New batter?");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                strike_count = 0;
                                ball_count = 0;
                                // Note, you have to call update count here because.
                                //   the call builder.show() below is non blocking.
                                updateBalls();
                                updateStrikes();
                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // Do nothing
                                finish();
                            }
                        });
                        builder.show();
                    }

                    mode.finish(); // Action picked, so close the CAB
                    return true;
                default:
                    return false;
            }
        }
        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };
*/



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "Starting onCreate...");
        setContentView(R.layout.content_main);

        if (savedInstanceState != null) {
            out_count = savedInstanceState.getInt("out_count");
        }

        View strikeButton = findViewById(R.id.strike_button);
        strikeButton.setOnClickListener(this);

        View ballButton = findViewById(R.id.ball_button);
        ballButton.setOnClickListener(this);

        SharedPreferences settings = getSharedPreferences(PREFERENCE_NAME, 0);
        out_count = settings.getInt("out_count", 0);
        updateStrikes();
        updateBalls();
        updateOuts();
    }

    private void updateStrikes() {
        TextView t = (TextView)findViewById(R.id.strike_value);
        t.setText(Integer.toString(strike_count));
    }

    private void updateBalls() {
        TextView t = (TextView)findViewById(R.id.ball_value);
        t.setText(Integer.toString(ball_count));
    }

    private void updateOuts() {
        TextView t = (TextView)findViewById(R.id.out_value);
        t.setText(Integer.toString(out_count));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.strike_button:
                strike_count++;
                if (strike_count == 3) {
                    out_count++;
                    // Builder is an inner class so we have to qualify it
                    // with its outer class: AlertDialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    //builder.setTitle("Dialog box");
                    builder.setMessage("3 strikes, out! New batter?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            strike_count = 0;
                            ball_count = 0;
                            // Note, you have to call update count here because.
                            //   the call builder.show() below is non blocking.
                            updateBalls();
                            updateStrikes();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Do nothing
                            finish();
                        }
                    });
                    builder.show();
                }
                break;
            case R.id.ball_button:
                ball_count++;
                if (ball_count == 4) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    //builder.setTitle("Dialog box");
                    builder.setMessage("4 balls, walk. New batter?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            strike_count = 0;
                            ball_count = 0;
                            // Note, you have to call update count here because.
                            //   the call builder.show() below is non blocking.
                            updateBalls();
                            updateStrikes();
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // Do nothing
                            finish();
                        }
                    });
                    builder.show();
                }
                break;
        }
        updateBalls();
        updateStrikes();
        updateOuts();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()){
            case R.id.reset:
                out_count = 0;
                updateOuts();
                return true;
            case R.id.about:
                Intent intent = new Intent(this, AboutActivity.class);
                //intent.putExtra(EXTRA_DATA, "extra data or parameter you want to pass to activity");
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause()");
        SharedPreferences settings = getSharedPreferences(PREFERENCE_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("out_count", out_count);

        editor.commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()");
    }

    @Override
    protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);

        Log.i(TAG, "onSaveInstanceState()");
        icicle.putInt("out_count", out_count);
        icicle.putInt("ball_count", ball_count);
        icicle.putInt("strike_count", strike_count);
    }

    @Override
    protected void onRestoreInstanceState(Bundle icicle) {
        super.onRestoreInstanceState(icicle);
        ball_count = icicle.getInt("ball_count");
        strike_count=icicle.getInt("strike_count");
        Log.i(TAG, "onRestoreInstanceState()");
    }


}
